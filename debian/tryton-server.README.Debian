tryton-server for Debian
========================

Setup
=====

Preparing the database
----------------------

Tryton uses a Relational Database Management System (RDBMS) to store data. The
preferred RDBMS for Tryton is PostgreSQL.

You have to setup this database manually. Here is a short explanation how to
achieve this (you need to execute all commands as root):

  * Making sure, PostgreSQL is running:

    $ sudo service postgresql* restart

    Note: If PostgreSQL runs on another machine than the Tryton server, make sure
    you have setup database password authentication. Please refer to the
    PostgreSQL manual how to do this. If they run on the same machine, connection
    via local sockets is preferred and you are done with the default configuration
    of the PostgreSQL package.

  * Creating the database user:

    $ sudo -u postgres createuser --createdb --no-createrole --no-superuser \
        --pwprompt tryton

    You have to enter
    * a password for the future database user (this will be used later in the
      setup of the database URI in /etc/tryton/trytond.conf as password)
    * confirm it
    * and finally enter the password of the postgres superuser.

    Note: If you want to run the database as another user than 'tryton', you
    need to replace 'tryton' above with the user you want to use instead.

Preparing the Tryton server
---------------------------

  * Setting up the Tryton server:

    Note: The server comes with three binaries:
    - trytond (runs the server(s))
    - trytond-admin (used for the adiminstration tasks of the server, e.g. the
      database administration)
    - trytond-cron (runs the cron part of the server, should only be started
      once per configuration)
      SysVinit:
      - Configure the databases you wish to run the cron job on in /etc/default/tryton-server
      - Enable the init script with update-rc.d tryton-server-cron enable
      Systemd:
      - Enable the cron job for <database> with
        systemctl enable tryton-server-cron@<database>.service

    Adjust /etc/tryton/trytond.conf to reflect the setup of your system by using
    the database user and password from step 1 for the database URI.

  * If the Tryton server shall listen on some external interface (i.e. shall be
    available for clients connecting from other machines), change the listen
    parameter in the web section accordingly.

  * If the Tryton server is listening on external interfaces, it is highly
    recommended to enable SSL for the connection.

    Note: The package is prepared to use the snakeoil certfificates from
    the ssl-cert package. If you are installing the ssl-cert package after
    the tryton-server package, take care to add the tryton user to the
    ssl-cert group with

    $ sudo adduser tryton ssl-cert

  * Restarting trytond:

    $ sudo service tryton-server restart
    $ sudo service tryton-server-cron restart

    Note: The fingerprint of connected servers is stored in the clients
    known_hosts file. When a server is changed for its SSL usage, the client
    will refuse to reconnect to this server. Removing the according line
    from ~/.config/tryton/<version>/known_hosts will allow one to connect to
    the server again.


Now you are ready to connect with a client, e.g. tryton-client.

Creating the database
---------------------

On the command line:

  * Creating the database:

    $ sudo -u postgres createdb --encoding=UNICODE --owner=tryton tryton

    Note: Use your database user for tryton as owner of the database, and enter
    a name for the database (here as default: tryton).

  * Initializing the database:

    $ sudo -u tryton trytond-admin -v -c /etc/tryton/trytond.conf --all -d <your_database_name>

    Note: Use the database name you chose in the previous step (here as default: tryton).
    You will be asked for the admin password for this database.

    As result you will have a bare Tryton database with the base modules installed.
  
  * Activating a new language on an existing database:

    $ sudo -u tryton trytond-admin -v -c /etc/tryton/trytond.conf --all -d <your_database_name> -l <language_code>

    Available language codes are at the time of writing:
    en, bg, ca, cs, de, es, es_419, fr, hu_HU, it_IT, lo, lt, nl, pt_BR, ru, sl, zh_CN

Upgrade
-------

  * Once you have upgraded to a new major version (second number of the
    version string) you have to update your database(s).

    While usually migrations between major versions are fully supported, it is
    always a good idea to check the migration page of the upstream project at
    https://discuss.tryton.org/c/migration for any manual action to take before or
    after the upgrade process.
    
    After the categorically recommended backup do:

    $ sudo -u tryton trytond-admin -v -c /etc/tryton/trytond.conf --all -d <your_database_name>

    Remember to replace tryton with the name of your database.


Notes
-----

Now, you're finished with the system setup. Please be aware of the following things:

  * Each Tryton database will have its own admin with login password
    stored in the database itself.

  * trytond must have read access to its configuration file, otherwise it will
    start with internal defaults. The postinst script will (re)set ownership to
    the system user running trytond and correct the permissions on the standard
    configuration file (/etc/tryton/trytond.conf), if not otherwise stated by
    means of dpkg-statoverride.

  * trytond listens by default on port 8000 (jsonrpc). If you need to change
    this, edit /etc/tryton/trytond.conf in the section [web].

  * trytond in its upstream configuration listens by default to the localhost
    interface. If you want to change this default to listen on all interfaces,
    edit /etc/tryton/trytond.conf in the section [web].

  * Activation of modules in the database can be done from the
    Administration Panel of the client. Under Modules you can select from the
    modules packages (trytond-modules*) that you have installed on your system.

  * Only the same major version of Tryton client and Tryton server can connect.

  * The start of Tryton cron has to be enabled and configured with the target
    database with the start scripts (sysvinit, systemd).

  * The start of Tryton workers has to be enabled and configured with the target
    database in the configuration file (s. /etc/trytond.conf) *and*
    with the start scripts (sysvinit, systemd).

 -- Mathias Behrle <mbehrle@debian.org>  Thu, 19 Nov 2018 12:00:00 +0100
